export interface User {
  email: string;
  password: string;
}

export interface LaravelAuthResponse {
  access_token: string;
  expires_in: string;
  token_type: string;
  user?: string;
}
