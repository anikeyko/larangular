import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {LaravelAuthResponse, User} from '../interfaces';
import {environment} from '../../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import {Subject, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public error$: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient) { }

  get token(): string {
    const expDate = new Date(localStorage.getItem('auth-token-exp'));
    if (new Date() > expDate) {
      this.logout();
      return null;
    }
    return localStorage.getItem('auth-token');
  }

  logout() {
    this.setToken(null);
  }


  login(user: User) {
    return this.http.post(`${environment.apiUrl}/api/login`, user)
      .pipe(
        tap(this.setToken),
        catchError(this.handleError.bind(this))
      );
  }

  handleError(error: HttpErrorResponse) {
    const message = error.error.error;

    switch (message) {
      case 'Unauthorized':
        this.error$.next('Неверный пароль');
        break;
      case 'Unauthorized2':
        this.error$.next('Неверный пароль');
        break;
    }

    return throwError(error);
  }

  private setToken(response: LaravelAuthResponse | null) {

    if (response) {
      const expDate = new Date(new Date().getTime() + +response.expires_in * 1000);
      localStorage.setItem('auth-token', response.access_token);
      localStorage.setItem('auth-token-exp', expDate.toString());
    } else {
      localStorage.clear();
    }
  }

  isAutheticated(): boolean {
    return !!this.token;
  }
}
